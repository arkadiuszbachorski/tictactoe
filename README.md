# Tic Tac Toe

TicTacToe game written with Node and TypeScript for educational purposes. First project in TypeScript ever.

## How to run it?

1. Clone or download repository

2. Install dependencies -
    ```
    npm install
    ```
3. Build code -
    ```
    npm run build
    ```
4. Run -
    ```
    npn run play
    ```