import TicTacToe from './TicTacToe/TicTacToe';
import UI from './UI/UI';
import Board from './Board/Board';
import { Menu } from './Menu/Menu';

const menu = new Menu();
const { player1, player2 } = menu.getPlayers();

const game = new TicTacToe(new UI(), player1, player2, new Board());
game.mainLoop();
