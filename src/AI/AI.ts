import { BoardInterface } from '../Board/Board';
import { PlayerCharacter } from '../Player/Player';
import randomInt from '../utils/randomInt';
import shuffleArray from '../utils/shuffleArray';

interface AIInterface {
    getIndex(): number;
}

interface GroupedBoard {
    X: number[];
    O: number[];
    empty: number[];
}

export default class AI implements AIInterface {
    board: BoardInterface;
    userCharacter: PlayerCharacter;
    aiCharacter: PlayerCharacter;
    groupedBoard: GroupedBoard[];

    constructor(board: BoardInterface, userCharacter: PlayerCharacter, aiCharacter: PlayerCharacter) {
        this.board = board;
        this.userCharacter = userCharacter;
        this.aiCharacter = aiCharacter;
    }

    getIndex() {
        if (this.board.isEmpty()) {
            return this.firstRandomPlacement();
        } else {
            this.groupBoard();
            return this.getWinningIndex() || this.getDefensiveIndex() || this.getAttackingIndex() || this.getAnyIndex();
        }
    }

    protected groupBoard(): void {
        const finalData: GroupedBoard[] = [];

        this.board.iterate((indexes, items) => {
            const data: GroupedBoard = {
                X: [],
                O: [],
                empty: [],
            };

            items.forEach((item, index) => {
                let computedProp = 'empty';
                if (item === this.userCharacter) {
                    computedProp = this.userCharacter;
                } else if (item === this.aiCharacter) {
                    computedProp = this.aiCharacter;
                }
                data[computedProp].push(indexes[index]);
            });

            if (data.empty.length !== 0) {
                finalData.push(data);
            }
        });

        this.groupedBoard = shuffleArray(finalData);
    }

    protected firstRandomPlacement() {
        const inTheMiddle = Math.random() < 0.33;
        if (inTheMiddle) {
            return 4;
        } else {
            return randomInt(0, this.board.fields.length - 1);
        }
    }

    protected getWinningIndex(): false | number {
        for (const item of this.groupedBoard) {
            if (item.empty.length === 1 && item[this.aiCharacter].length === 2) {
                return item.empty[0];
            }
        }

        return false;
    }

    protected getDefensiveIndex(): false | number {
        for (const item of this.groupedBoard) {
            if (item.empty.length === 1 && item[this.userCharacter].length === 2) {
                return item.empty[0];
            }
        }

        return false;
    }

    protected getAttackingIndex(): false | number {
        for (const item of this.groupedBoard) {
            if (item.empty.length === 2 && item[this.aiCharacter].length === 1) {
                return item.empty[randomInt(0, item.empty.length - 1)];
            }
        }
        return false;
    }

    protected getAnyIndex(): number {
        for (const item of this.groupedBoard) {
            if (item.empty.length !== 0) {
                return item.empty[randomInt(0, item.empty.length - 1)];
            }
        }
    }
}
