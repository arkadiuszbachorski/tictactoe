import Player, { PlayerInterface } from '../Player/Player';
import IO from '../IO/IO';
import UI from '../UI/UI';
import WinnerChecker from '../WinnerChecker/WinnerChecker';
import { BoardInterface } from '../Board/Board';
import AI from '../AI/AI';

export type PlayerNumber = 1 | 2;

export default class TicTacToe {
    board: BoardInterface;
    player1: PlayerInterface;
    player2: PlayerInterface;
    currentTurn: PlayerNumber = 1;
    gameLoop = true;
    ui: UI;

    constructor(ui: UI, player1: PlayerInterface, player2: PlayerInterface, board: BoardInterface) {
        this.ui = ui;
        this.player1 = player1;
        this.player2 = player2;
        this.board = board;
    }

    mainLoop() {
        this.board.reset();
        this.gameLoop = true;
        while (this.gameLoop) {
            this.ui.print(this.board, this.getCurrentPlayer());
            const index = this.nextMove();
            this.board.setField(index, this.getCurrentPlayer().character);
            const winningFields = new WinnerChecker(this.board, this.getCurrentPlayer().character).checkForWinner();
            if (winningFields) {
                IO.clear();
                this.board.setWinningColors(winningFields);
                this.ui.printBoard(this.board, false);
                this.updateWins();
                this.askIfPlayAgain();
            } else if (this.board.isFull()) {
                IO.clear();
                this.ui.printBoard(this.board);
                this.ui.printDraw();
                this.askIfPlayAgain();
            }
            this.updateTurn();
        }
    }

    protected askIfPlayAgain() {
        this.gameLoop = false;
        this.ui.printAskIfPlayAgain();
        const playAgain = IO.get('\n') === 'y';
        if (playAgain) {
            this.mainLoop();
        } else {
            IO.clear();
            this.ui.printScoreTable([this.player1, this.player2]);
            this.ui.printExit();
        }
    }

    protected updateWins() {
        this.getCurrentPlayer().won();
        this.ui.printWinner(this.getCurrentPlayer());
        this.ui.printScoreTable([this.player1, this.player2]);
    }

    protected getCurrentPlayer(): Player {
        return this[`player${this.currentTurn}`];
    }

    protected nextMove() {
        if (this.getCurrentPlayer().isAi) {
            const ai = new AI(this.board, this.player1.character, this.player2.character);
            return ai.getIndex();
        } else {
            return this.askForFieldNumber();
        }
    }

    protected askForFieldNumber() {
        let index: number;
        do {
            index = parseInt(IO.get('\n\nField number: '), 10);
        } while (isNaN(index) || index < 1 || index > 9 || !this.board.isFieldEmpty(index - 1));

        return index - 1;
    }

    protected updateTurn() {
        this.currentTurn = this.currentTurn === 1 ? 2 : 1;
    }
}
