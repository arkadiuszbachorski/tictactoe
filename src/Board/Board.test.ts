import Board from './Board';
import { Colors } from '../IO/IO';

const board = new Board();

beforeEach(() => {
    board.reset();
});

it('sets fields to empty', () => {
    expect(board.fields.every(item => item === null)).toBe(true);
    expect(board.colors.every(item => item === Colors.FgYellow)).toBe(true);
});

it('checks if board is full', () => {
    board.fields = new Array(9).fill('X');
    expect(board.isFull()).toBe(true);

    board.fields[2] = null;
    expect(board.isFull()).toBe(false);
});

it('checks if board is empty', () => {
    expect(board.isEmpty()).toBe(true);

    board.fields[2] = 'X';
    expect(board.isFull()).toBe(false);
});

it('checks if field is empty', () => {
    board.fields = new Array(9).fill('X');
    board.fields[1] = null;

    expect(board.isFieldEmpty(1)).toBe(true);
    expect(board.isFieldEmpty(2)).toBe(false);
});

it('sets many colors', () => {
    expect(board.colors[0]).toBe(Colors.FgYellow);
    expect(board.colors[1]).toBe(Colors.FgYellow);

    board.setWinningColors([0, 1]);

    expect(board.colors[0]).toBe(Colors.FgGreen);
    expect(board.colors[1]).toBe(Colors.FgGreen);
});

it('sets field and updated color', () => {
    expect(board.fields[0]).toBe(null);
    expect(board.colors[0]).toBe(Colors.FgYellow);

    board.setField(0, 'X');

    expect(board.fields[0]).toBe('X');
    expect(board.colors[0]).toBe(Colors.FgWhite);
});

it('can iterate', () => {
    board.fields = ['X', 'O', 'X', 'X', 'O', 'X', 'X', 'O', 'X'];
    board.iterate((indexes, values) => {
        expect(values).toEqual(['X', 'O', 'X']);
        return true;
    });
});
