import { PlayerCharacter } from '../Player/Player';
import { Colors } from '../IO/IO';

export type BoardCharacter = PlayerCharacter | null;
export type BoardValues = BoardCharacter[];
export type BoardColors = Colors[];

export type IterationCallback = (indexes: number[], values: BoardCharacter[]) => void | boolean;

export interface BoardInterface {
    fields: BoardValues;
    colors: BoardColors;
    reset(): void;
    isFull(): boolean;
    isEmpty(): boolean;
    isFieldEmpty(index: number): boolean;
    setField(index: number, character: PlayerCharacter): void;
    setWinningColors(indexes: number[]): void;
    iterate(callback: IterationCallback): void;
}

export default class Board implements BoardInterface {
    fields: BoardValues;
    colors: BoardColors;

    reset() {
        this.fields = new Array(9).fill(null);
        this.colors = new Array(9).fill(Colors.FgYellow);
    }

    isFull() {
        return !this.fields.includes(null);
    }

    isEmpty() {
        return this.fields.every(item => item === null);
    }

    isFieldEmpty(index: number) {
        return this.fields[index] === null;
    }

    setField(index: number, character: PlayerCharacter) {
        this.fields[index] = character;
        this.colors[index] = Colors.FgWhite;
    }

    setWinningColors(indexes: number[]) {
        indexes.forEach(item => {
            this.colors[item] = Colors.FgGreen;
        });
    }

    protected mapIndexesToFields(indexes: number[]): BoardValues {
        return indexes.map(index => this.fields[index]);
    }

    iterate(callback: IterationCallback) {
        let indexes;
        let values;

        indexes = [0, 4, 8];
        values = this.mapIndexesToFields(indexes);
        if (callback(indexes, values)) return;

        indexes = [2, 4, 6];
        values = this.mapIndexesToFields(indexes);
        if (callback(indexes, values)) return;

        for (let i = 0; i < 3; i++) {
            indexes = [i * 3, i * 3 + 1, i * 3 + 2];
            values = this.mapIndexesToFields(indexes);
            if (callback(indexes, values)) break;

            indexes = [i, i + 3, i + 6];
            values = this.mapIndexesToFields(indexes);
            if (callback(indexes, values)) break;
        }
    }
}
