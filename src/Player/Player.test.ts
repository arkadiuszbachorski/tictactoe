import Player from './Player';

it('increments wins properly', () => {
    const player = new Player('X', 'Lorem');
    expect(player.wins).toBe(0);
    player.won();
    expect(player.wins).toBe(1);
});
