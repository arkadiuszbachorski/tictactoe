export type PlayerCharacter = 'X' | 'O';

export interface PlayerInterface {
    character: PlayerCharacter;
    name: string;
    wins: number;
    isAi: boolean;
    won(): void;
}

export default class Player implements PlayerInterface {
    character: PlayerCharacter;
    name: string;
    wins: number = 0;
    isAi: boolean;

    constructor(character: PlayerCharacter, name: string, isAi: boolean = false) {
        this.character = character;
        this.name = name;
        this.isAi = isAi;
    }

    won() {
        this.wins += 1;
    }
}
