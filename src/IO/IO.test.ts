import IO, { Colors } from './IO';
import rl from 'readline-sync';

process.stdout.write = jest.fn();
console.clear = jest.fn();
rl.question = jest.fn();
rl.question.mockImplementationOnce(() => {
    return 'userInput';
});

afterEach(() => {
    jest.clearAllMocks();
});

it('clears the console', () => {
    IO.clear();
    expect(console.clear).toBeCalledTimes(1);
});

it('gets called with user input', () => {
    const message = 'Lorem ipsum';

    const val = IO.get(message);
    expect(rl.question).toBeCalledWith(message);
    expect(val).toBe('userInput');
});

it('prints simple message', () => {
    const message = 'Lorem ipsum';
    IO.print(message);
    expect(process.stdout.write).toBeCalledTimes(1);
    expect(process.stdout.write).toBeCalledWith(message);
});

it('prints coloured message', () => {
    const message = 'Lorem ipsum';
    IO.print(message, Colors.FgRed);
    expect(process.stdout.write).toBeCalledTimes(1);
    // Testing not here, because color should be injected
    expect(process.stdout.write).not.toBeCalledWith(message);
});
