import IO, { Colors } from '../IO/IO';
import Player, { PlayerInterface } from '../Player/Player';

type PlayersObject = {
    player1: PlayerInterface;
    player2: PlayerInterface;
};

export class Menu {
    withAi: boolean;
    player1: PlayerInterface;
    player2: PlayerInterface;

    getPlayers(): PlayersObject {
        this.askIfWantsToPlayWithAi();
        this.askForPlayerNames();
        return {
            player1: this.player1,
            player2: this.player2,
        };
    }

    protected askIfWantsToPlayWithAi() {
        IO.clear();
        IO.print('Would you like to play with computer?');
        IO.print(' [y/n]', Colors.FgYellow);
        this.withAi = IO.get('\n') === 'y';
    }

    protected askForPlayerNames() {
        IO.clear();
        if (this.withAi) {
            this.player1 = new Player('X', IO.get('Player name: '));
            this.player2 = new Player('O', 'EvilCorp', true);
        } else {
            this.player1 = new Player('X', IO.get('First player name: '));
            this.player2 = new Player('O', IO.get('Second player name: '));
        }
    }
}
