import { Menu } from './Menu';
import IO from '../IO/IO';

let menu;

// prevent displaying anything in console
IO.print = jest.fn();

beforeEach(() => {
    menu = new Menu();
    jest.clearAllMocks();
});

it('creates normal player and player with ai', () => {
    IO.get = jest
        .fn()
        .mockReturnValueOnce('y')
        .mockReturnValueOnce('Lorem');

    const { player1, player2 } = menu.getPlayers();

    expect(player1.name).toBe('Lorem');
    expect(player1.isAi).toBe(false);
    expect(player2.isAi).toBe(true);
});

it('creates two normal players', () => {
    IO.get = jest
        .fn()
        .mockReturnValueOnce('f')
        .mockReturnValueOnce('Lorem')
        .mockReturnValueOnce('Ipsum');

    const { player1, player2 } = menu.getPlayers();

    expect(player1.name).toBe('Lorem');
    expect(player2.name).toBe('Ipsum');
    expect(player1.isAi).toBe(false);
    expect(player2.isAi).toBe(false);
});
