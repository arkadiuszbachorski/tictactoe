import IO, { Colors } from '../IO/IO';
import { PlayerInterface } from '../Player/Player';
import { BoardInterface, BoardCharacter } from '../Board/Board';

export interface UIInterface {
    printDraw(): void;

    printBoard(board: BoardInterface, autoFillCharacter: boolean): void;

    printAskIfPlayAgain(): void;

    printAskIfPlayAgain();

    printCurrentPlayer(player: PlayerInterface): void;

    printWinner(player: PlayerInterface): void;

    printScoreTable(players: PlayerInterface[]): void;

    print(board: BoardInterface, currentPlayer: PlayerInterface): void;
}

export default class UI implements UIInterface {
    protected paddingSize: number = 1;

    protected repeatTimes(character: string, times?: number): string {
        return Array((times ?? this.paddingSize) + 1).join(character);
    }

    protected paddTop() {
        IO.print(this.repeatTimes('\n'));
    }

    protected paddLeft() {
        IO.print(this.repeatTimes(' ', this.paddingSize + 1));
    }

    protected printCharacter(character: BoardCharacter, color: Colors, index: number, autoFillCharacter: boolean) {
        if (typeof character === 'string') {
            IO.print(` ${character} `, color);
        } else if (autoFillCharacter) {
            IO.print(` ${index + 1} `, color);
        } else {
            IO.print(`   `);
        }
    }

    printDraw() {
        IO.print(`\nDraw!`, Colors.FgGreen);
    }

    printBoard(board: BoardInterface, autoFillCharacter: boolean = true) {
        this.paddTop();
        board.fields.forEach((item, index) => {
            if (index % 3 === 0) {
                this.paddLeft();
            }
            this.printCharacter(item, board.colors[index], index, autoFillCharacter);
            if (index % 3 !== 2) {
                IO.print('|');
            } else {
                IO.print('\n');
                if (index + 1 < 8) {
                    this.paddLeft();
                    IO.print('-----------');
                    IO.print('\n');
                }
            }
        });
    }

    printAskIfPlayAgain() {
        IO.print('\nWould you like to play again?');
        IO.print(' [y/n]', Colors.FgYellow);
    }

    printCurrentPlayer(player: PlayerInterface) {
        IO.print(`\n${player.name} turn`, Colors.Underscore);
    }

    printWinner(player: PlayerInterface) {
        IO.print(`\n${player.name} won!`, Colors.FgGreen);
    }

    printScoreTable(players: PlayerInterface[]) {
        IO.print('\n');
        players.forEach(player => {
            IO.print(`\n${player.name} wins: ${player.wins}`);
        });
    }

    printExit() {
        IO.print('\n\n');
        IO.print('Thank you for playing!');
        IO.print('\n\n');
    }

    print(board: BoardInterface, currentPlayer: PlayerInterface) {
        IO.clear();
        this.printBoard(board);
        this.printCurrentPlayer(currentPlayer);
    }
}
