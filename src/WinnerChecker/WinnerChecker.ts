import { PlayerCharacter } from '../Player/Player';
import { BoardCharacter, BoardInterface } from '../Board/Board';

export interface WinnerCheckerInterface {
    board: BoardInterface;
    character: PlayerCharacter;
    checkForWinner(): CheckReturn;
}
type CheckReturn = false | number[];

export default class WinnerChecker implements WinnerCheckerInterface {
    board: BoardInterface;
    character: PlayerCharacter;

    constructor(board: BoardInterface, character: PlayerCharacter) {
        this.board = board;
        this.character = character;
    }

    protected checkIfValuesAreEqualCharacters(indexes: BoardCharacter[]) {
        return indexes.every(item => item === this.character);
    }

    checkForWinner(): CheckReturn {
        let value: CheckReturn = false;

        this.board.iterate((indexes, characters) => {
            if (this.checkIfValuesAreEqualCharacters(characters)) {
                value = indexes;
                return true;
            }
        });

        return value;
    }
}
