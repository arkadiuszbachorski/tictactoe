import WinnerChecker from './WinnerChecker';
import Board from '../Board/Board';
const character = 'X';

const board = new Board();

it('checks rows properly', () => {
    board.fields = ['X', 'X', 'X', null, null, null, null, null, null];
    const indexes = new WinnerChecker(board, character).checkForWinner();
    expect(indexes).toEqual([0, 1, 2]);
});

it('checks columns properly', () => {
    board.fields = [null, 'X', null, null, 'X', null, null, 'X', null];
    const indexes = new WinnerChecker(board, character).checkForWinner();
    expect(indexes).toEqual([1, 4, 7]);
});

it('checks diagonally', () => {
    board.fields = ['X', null, null, null, 'X', null, null, null, 'X'];
    const indexes = new WinnerChecker(board, character).checkForWinner();
    expect(indexes).toEqual([0, 4, 8]);
});

it('returns false when there is no winner', () => {
    board.fields = [null, null, null, null, null, null, null, null, null];
    const indexes = new WinnerChecker(board, character).checkForWinner();
    expect(indexes).toBe(false);
});
